//
//  SecondViewController.m
//  test_textview
//
//  Created by Антон Кудряшов on 20/06/16.
//  Copyright © 2016 Антон Кудряшов. All rights reserved.
//

#import "SecondViewController.h"

#import <MZFormSheetPresentationViewController.h>

@interface SecondViewController () <MZFormSheetPresentationContentSizing>

@end

@implementation SecondViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    NSLog(@"p %s", __PRETTY_FUNCTION__);
}
- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    NSLog(@"p %s", __PRETTY_FUNCTION__);
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    NSLog(@"p %s", __PRETTY_FUNCTION__);
}
- (void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:animated];
    NSLog(@"p %s", __PRETTY_FUNCTION__);
}

- (IBAction)dismiss:(id)sender
{
    [UIView animateWithDuration:0.2 animations:^{
    [[self mz_formSheetPresentingPresentationController].presentationController layoutPresentingViewController];

        
    }];    //[self dismissViewControllerAnimated:YES completion:nil];
}

- (BOOL)shouldUseContentViewFrameForPresentationController:(__kindof MZFormSheetPresentationController *)presentationController
{
    return YES;
}
- (CGRect)contentViewFrameForPresentationController:(__kindof MZFormSheetPresentationController *)presentationController currentFrame:(CGRect)currentFrame
{
    currentFrame.size.height = 100 + arc4random()%500;
    return currentFrame;
}

@end
