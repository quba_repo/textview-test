//
//  AppDelegate.h
//  test_textview
//
//  Created by Антон Кудряшов on 15/06/16.
//  Copyright © 2016 Антон Кудряшов. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

