//
//  ViewController.m
//  test_textview
//
//  Created by Антон Кудряшов on 15/06/16.
//  Copyright © 2016 Антон Кудряшов. All rights reserved.
//

#import "ViewController.h"

#import <MZFormSheetPresentationViewControllerSegue.h>

@interface ViewController () <UITextViewDelegate>

@property (nonatomic, weak) IBOutlet UITextView* textView;

@end

@implementation ViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    NSString* str = @"The deepest place in the world";
    NSMutableAttributedString* attrubutedText = [[NSMutableAttributedString alloc] initWithString:str];

    [attrubutedText addAttribute:NSLinkAttributeName value:@"profile://userID/23234455"
                           range:(NSRange){str.length - 5, 5}];
    
    self.textView.attributedText = attrubutedText;
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    NSLog(@"%s", __PRETTY_FUNCTION__);
}
- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    NSLog(@"%s", __PRETTY_FUNCTION__);
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    NSLog(@"%s", __PRETTY_FUNCTION__);
}
- (void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:animated];
    NSLog(@"%s", __PRETTY_FUNCTION__);
}


-      (BOOL)textView:(UITextView *)textView
shouldInteractWithURL:(NSURL *)URL
              inRange:(NSRange)characterRange
{
    if ([URL.scheme isEqualToString:@"profile"]) {
        [self performSegueWithIdentifier:@"SEG" sender:nil];
        return NO;
    }
    return YES;
}

- (void)prepareForSegue:(MZFormSheetPresentationViewControllerSegue *)segue sender:(id)sender
{
    MZFormSheetPresentationViewController* dest = segue.formSheetPresentationController;
    dest.contentViewControllerTransitionStyle = MZFormSheetPresentationTransitionStyleSlideFromBottom;
}

- (IBAction)exit:(UIStoryboardSegue*)sender
{

}

@end
